from game_engine import init, Game, Layer, Sprite
from random import randint
import custom_item


resolution = (800, 600)
init(resolution, "The Best Game Ever")
game = Game()

layer = Layer()
game.add(layer)


for i in range(10):
    position = randint(0, 800), randint(0, 600)
    sprite = Sprite(r'assets/bullet.png', position)
    layer.add(sprite)


game.run()