from game_engine import Sprite


RESOLUTION = 800, 600
GRAVITY = 0, -100

class Bullet(Sprite):
    def __init__(self, position, speed):
        path = r'assets/bullet.png'
        super().__init__(path,position)
        self.speed = speed

    def update(self, dt):
        delta_position = self.speed[0] * dt, self.speed[1] * dt
        position = self.position

        position = position[0] + delta_position[0], position[1] + delta_position[1]
        self.position = position

        if position[0] <= 0 or position[0] >= RESOLUTION[0]:
            self.speed = self.speed[0] * -1, self.speed[1]
        
        if position[1] <= 0 or position[1] >= RESOLUTION[1]:
            self.speed = self.speed[0], self.speed[1] * -1

        #gravity
        self.speed = self.speed[0] + GRAVITY[0] * dt, self.speed[1] + GRAVITY[1] * dt

        super().update(dt)

    def on_collision(self, other):
        if isinstance(other, Bullet):
            pass
        else:
            other.destroy()
        super().on_collision(other)

    def on_key_press(self, symbol, modifiers):
        pass

    def on_key_release(self, symbol, modifiers):
        pass



class Cannon(Sprite):
    def __init__(self, image_path, position, bullet_origin, bullet_sens, trigger_key):
        super().__init__(image_path, position)
        self.bullet_origin = bullet_origin
        self.bullet_sens = bullet_sens
        self.trigger_key = trigger_key
        self.power = 0
        self.power_on = False
        self.can_shoot = True
        self.timer = 0


    def on_key_press(self, symbol, modifiers):
        if symbol == self.trigger_key:
            if self.can_shoot:
                self.power_on = True


    def update(self, dt):
        if self.power_on:
            self.power += dt * 10

        if not self.can_shoot:
            self.timer += dt
            if self.timer >= 0.5:
                self.can_shoot = True
                self.timer = 0

        super().update(dt)


    def on_key_release(self, symbol, modifiers):
        if symbol == self.trigger_key:
            if self.can_shoot and self.power > 0:
                self.power_on = False
                self.can_shoot = False

                bullet_speed = 100, 100
                bullet_speed = bullet_speed[0] * self.bullet_sens[0], bullet_speed[1] * self.bullet_sens[1]
                bullet_speed = bullet_speed[0] * self.power, bullet_speed[1] * self.power

                self.power = 0

                bullet = Bullet(self.bullet_origin, bullet_speed)
                self.layer.add(bullet)

        
