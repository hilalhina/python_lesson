from custom_item import RESOLUTION, Cannon
from game_engine import init, Game, Layer
from pyglet.window import key

resolution = (RESOLUTION)
init(resolution, "The Best Game Ever")
game = Game()
#game.debug = True

layer = Layer()
game.add(layer)

cannon = Cannon(
    r'assets/cannon.png', 
    position=(0,0), 
    bullet_origin=(65,65), 
    bullet_sens=(1,1), 
    trigger_key=key.Q)

layer.add(cannon)

cannon_2 = Cannon(
    r'assets/cannon2.png',
    position=(740, 0),
    bullet_origin=(725,65),
    bullet_sens=(-1,1),
    trigger_key=key.M)

layer.add(cannon_2)

game.run()